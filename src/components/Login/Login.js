import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import './Login.css';

export default function Login({ setToken }) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [message, setMessage] = useState("");

    const handleSubmit = async e => {
        e.preventDefault();
        const auth_token = await loginUser({
            username,
            password
        });
        if (auth_token !== undefined) {
            setToken(auth_token);
            window.location.reload();
        }
    }

    async function loginUser(credentials) {
        return fetch('https://vankirillov.pythonanywhere.com/auth/token/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })
            .then(data => {
                if (data.ok) { return data.json() }
                throw new Error('Ошибка аутентификации');
            })
            .catch((error) => {
                setMessage(error.message);
            });
    }

    return (
        <div className="login-wrapper">
            <h1>Войти</h1>
            <Form onSubmit={handleSubmit}>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="admin" onChange={e => setUserName(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="admin" onChange={e => setPassword(e.target.value)} />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>

                <div className="message">{message ? <p>{message}</p> : null}</div>
            </Form>
        </div>
    )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
};