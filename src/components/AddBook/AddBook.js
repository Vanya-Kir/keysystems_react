import React, { useEffect, useState } from 'react';
import { getAuthor, getGenre } from '../Requests';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export default function AddBook() {
    const [name, setName] = useState("");
    const [author, setAuthor] = useState([]);
    const [genre, setGenre] = useState([]);
    const [description, setDescription] = useState("");
    const [message, setMessage] = useState("");


    const [authorInput, setAuthorInput] = useState("");
    const [genreInput, setGenreInput] = useState("");

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getAuthor();
            if (response.errors) {
                console.log(response.errors);
            }
            else {
                setAuthor(response);
                setAuthorInput(response[0])
            }
        }

        fetchInitialData();
    }, []);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getGenre();
            if (response.errors) {
                console.log(response.errors);
            }
            else {
                setGenre(response);
                setGenreInput(response[0])
            }
        }
        fetchInitialData();
    }, []);

    let handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const token = JSON.parse(sessionStorage.getItem("auth_token")).auth_token;
            let res = await fetch("https://vankirillov.pythonanywhere.com/api/v1/book/", {
                method: "POST",
                body: JSON.stringify({
                    name: name,
                    author: [authorInput],
                    genre: [genreInput],
                    description: description,
                }),
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + token,
                }),
            });

            if (res.status === 200 || res.status === 201) {
                // setAuthorInput(author);
                // setGenreInput(genre);
                setName("");
                // setAuthorInput("");
                // setGenreInput("");
                setDescription("");
                setMessage("Книга успешно создана");
            } else {
                setMessage("Ошибка");
            }
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div className="App">
            <Form onSubmit={handleSubmit}>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Название</Form.Label>
                    <Form.Control name="name" type="text" onChange={(e) => setName(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Автор</Form.Label>
                    <Form.Select name="author" onChange={(e) => setAuthorInput(e.target.value)}>
                        {author.map((value) => (
                            <option value={value} key={value}>
                                {value}
                            </option>
                        ))}
                    </Form.Select>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Жанр</Form.Label>
                    <Form.Select name="genre" onChange={(e) => setGenreInput(e.target.value)}>
                        {genre.map((value) => (
                            <option value={value} key={value}>
                                {value}
                            </option>
                        ))}
                    </Form.Select>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Описание</Form.Label>
                    <Form.Control name="description" as="textarea" rows={3} onChange={(e) => setDescription(e.target.value)} />
                </Form.Group>

                <Button type="submit">Создать</Button>

                <div className="message">{message ? <p>{message}</p> : null}</div>
            </Form>
        </div>
    );
}
