import React from 'react';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Link,
// } from "react-router-dom";
import Button from 'react-bootstrap/Button';

import './App.css';
// import AddBook from '../AddBook/AddBook';
import Login from '../Login/Login';
import CardList from '../CardList/CardList';
import useToken from './useToken';


function App() {
  const { auth_token, setToken } = useToken();

  function handleClick() {
    sessionStorage.clear();
    window.location.href = '/';
  }

  if (!auth_token) {
    return <Login setToken={setToken} />
  }

  return (
    <div className="wrapper">
      <div className="d-flex justify-content-between">
        <h1>Кириллов Иван</h1>
        <Button onClick={handleClick}>
          Выйти
        </Button>
      </div>
      <hr />
      <CardList />

      {/* <Router>
        <div>
          <div className="d-flex flex-row">

            <div className="p-2"><Button onClick={handleClick}>
              Выйти
            </Button>
            </div>
            <div className="p-2">
              <Link to="/addbook">Добавить книгу</Link>
            </div>
            <div className="p-2">
              <Link to="/">Книги</Link>
            </div>
          </div>
          <hr />

          <Switch>
            <Route exact path="/addbook">
              <AddBook />
            </Route>
            <Route path="/">
              <CardList />
            </Route>
          </Switch>
        </div>
      </Router> */}
    </div>
  );
}

export default App;