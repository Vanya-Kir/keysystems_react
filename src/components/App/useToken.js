import { useState } from 'react';

export default function useToken() {
    const getToken = () => {
        const tokenString = sessionStorage.getItem('auth_token');
        const userToken = JSON.parse(tokenString);
        return userToken?.auth_token
    };

    const [auth_token, setToken] = useState(getToken());

    const saveToken = userToken => {
        sessionStorage.setItem('auth_token', JSON.stringify(userToken));
        setToken(userToken.auth_token);
    };

    return {
        setToken: saveToken,
        auth_token
    }
}