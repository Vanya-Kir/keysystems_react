import Card from 'react-bootstrap/Card';

function CardElement(props) {
    return (
        <Card className="m-1 p-2 bd-highlight col-example test">
            <Card.Img
                variant="top"
                src={`https://picsum.photos/id/${props.value.id}/500`}
            />
            <Card.Body>
                <Card.Title>
                    {props.value.name}
                </Card.Title>
                <Card.Text>
                    <strong>Авторы:</strong> {props.value.author.join(', ')}
                </Card.Text>
                <Card.Text>
                    <strong>Жанр:</strong> {props.value.genre.join(', ')}
                </Card.Text>
                <Card.Text>
                    <strong>Описание:</strong> {props.value.description}
                </Card.Text>
                <Card.Text>
                    <strong>Likes:</strong> {props.value.like_counter}
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

export default CardElement;
