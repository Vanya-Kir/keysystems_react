import axios from 'axios';

const url = axios.create({
    baseURL: 'https://vankirillov.pythonanywhere.com/api/v1/',
});
let token = '';
token = sessionStorage.getItem("auth_token") ? JSON.parse(sessionStorage.getItem("auth_token")).auth_token : "";



const config = {
    headers: { Authorization: `Token ${token}` }
};

export const getAuthor = () => {
    let result = url
        .get('/author',
            config
        )
        .then((response) => {
            const names = response.data.map(item => item.name);
            // console.log(names);
            return names;
        })
        .catch((error) => {
            return error;
        });

    return result;
};

export const getGenre = () => {
    let result = url
        .get('/genre',
            config)
        .then((response) => {
            const names = response.data.map(item => item.name);
            return names;
        })
        .catch((error) => {
            return error;
        });

    return result;
};

export const getCards = () => {
    let result = url
        .get(`/book`,
            config)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};