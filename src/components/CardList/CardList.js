import React, { useEffect, useState } from 'react';
import CardElement from '../Card/Card';
import { getCards } from '../Requests';
import Pagination from '../Pagination/Pagination';

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import AddBook from '../AddBook/AddBook';

function CardList() {
    const [data, setData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [recordsPerPage] = useState(2);

    async function fetchInitialData() {
        const response = await getCards();
        if (!response.errors) {
            setData(response);
        }
    }

    useEffect(() => {
        fetchInitialData();
    }, []);


    const [show, setShow] = useState(false);

    const handleClose = () => {
        fetchInitialData();
        return setShow(false);
    }
    const handleShow = () => setShow(true);

    const indexOfLastRecord = currentPage * recordsPerPage;
    const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
    const currentRecords = data.slice(indexOfFirstRecord, indexOfLastRecord);
    const nPages = Math.ceil(data.length / recordsPerPage);

    return (
        <div>
            <Button variant="primary" onClick={handleShow}>
                Добавить книгу
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Добавление книги</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <AddBook />
                </Modal.Body>
            </Modal>

            <div className="container mt-5">
                <div className="d-flex flex-row flex-wrap justify-content-between">
                    {currentRecords.map((item, i) => (
                        <CardElement value={item} key={i} />
                    ))}
                </div>

                <Pagination
                    nPages={nPages}
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                />
            </div>
        </div>
    );
}

export default CardList;
